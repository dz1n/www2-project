document.querySelector('#userinput').focus();

var input = document.querySelector("#userinput");
var ol = document.querySelector("ol");
var enter = document.querySelector("#enterButton");
var li = document.querySelectorAll('li');

function createButton(str) {
	var li = document.createElement("li");
	var button = document.createElement("button");
	button.textContent = "Usuń";
	//button.innerHTML = '<img src="img/trash-347.png"/>'
	li.textContent = str;
	ol.appendChild(li);
	li.appendChild(button);
	button.classList.add('todobutton');
}

function readFromLocalstorage() {
    tasks = JSON.parse(localStorage.getItem('planner'));
    for(var i=0; i < tasks.length; i++){
		createButton(tasks[i].text);
	}
}

//function for insert
function insertIntoList(){
	if(input.value.length > 0){
		createButton(input.value);
		//input.value = "";

        var newDate = new Date();
        id = newDate.getTime();

        tasks = JSON.parse(localStorage.getItem('planner'));

            if (tasks == null) {
                tasks = [];
            }

            var taskList = JSON.parse(localStorage.getItem('planner'));

            var newTask = {
                "id": id,
                "text": input.value
            }

            tasks.push(newTask);
            localStorage.setItem('planner', JSON.stringify(tasks));
        
        input.value = "";
	}
}

//enter button entry
enter.addEventListener('click', insertIntoList)
input.addEventListener('keypress', function(event){
	if(event.keyCode === 13){
		insertIntoList();
	}
})

//strikethrough for li items
ol.addEventListener('click', function(event){
	event.target.classList.toggle('done');
})

//delete button
document.addEventListener('click', function(event){
	if(event.target.classList.contains('todobutton')){
		var str = event.target.parentElement.innerHTML;
		var new_str = ""
		for (let i = 0; i < str.length; i++) {
			if (str[i] == "<")
				break;
			new_str += str[i]
		}

		var items = JSON.parse(localStorage.getItem("planner"));
		for (var i =0; i< items.length; i++) {
			if (items[i].text == new_str) {
				items.splice(i, 1);
			}
		}
		items = JSON.stringify(items);
		localStorage.setItem("planner", items);

		ol.removeChild(event.target.parentElement);	
	}
})

